import sys 
import os
sys.path.append(os.path.relpath("controllers/calculations"))

from controllers.calculations import *

class DataCapture():
    numbers = list()
    less = 0
    greater = 0
    between = 0

    def add(self,num):
        if type(num) is int:
            self.numbers.append( num )
        else:
            raise Exception( num + " not is an Integer" )    

    def build_stats(self):
        return Calculations(self.numbers)

capture = DataCapture()
capture.add(3)
capture.add(9)
capture.add(3)
capture.add(4)
capture.add(6)

stats = capture.build_stats()

print( stats.less(4) )
print( stats.between(3,6) )
print( stats.greater(4) )