class Calculations():

    numbers = list()

    def __init__(self, numbers):
        self.numbers = numbers

    def less(self, lessNum):
        count_less = 0
        for n in self.numbers:
            if n < lessNum:
                count_less += 1
        return count_less    

    def between(self, min, max):
        count_between = 0
        for n in self.numbers:
            if n >= min and n <= max:
                count_between += 1
        return count_between
    def greater(self, greater):
        count_greater = 0
        for n in self.numbers:
            if n > greater:
                count_greater += 1
        return count_greater 